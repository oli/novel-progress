# novel-progress

my silly scripts for tracking word count when writing a novel.

`md_wc` works with markdown files.

`nw_wc` is to be used with [novelWriter](https://github.com/vkbo/novelWriter) projects. updated to work with Novelwriter 2.x.

`wc_today` tells you how much you've written today.

`nw_wc_auto` is a simplified version that inspects NovelWriter's `sessions.jsonl` file to check the project's wordcount.

## DEPENDENCIES
[graph-cli](https://github.com/mcastorina/graph-cli) for the chart generation.

you can install it with pip: `pip install graph-cli`.

## HOW IT WORKS
1. edit the project path in the scripts.
2. write a lot (or a little).
3. run script when you're done writing for the day.
4. repeat last two steps every day.

check docs for graph-cli if you want to tweak the command for the chart generation. or comment out the graph-cli line if you don't want to use it at all.

## ISSUES
1. things might explode.
2. i'm a writer, not a coder. oops.
